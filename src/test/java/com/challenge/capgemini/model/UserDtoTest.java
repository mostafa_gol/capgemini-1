package com.challenge.capgemini.model;

import com.challenge.capgemini.dto.request.UserRequestDto;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
class UserDtoTest {

    private static UserRequestDto initCustomerDto() {
        LocalDate localDate = LocalDate.of(1990, 8, 19);
        return new UserRequestDto( "Adrian", "Pooji", localDate);
    }

    @Test
    void shouldCreateCustomerRequestDto() {

        UserRequestDto requestDto = initCustomerDto();
        assertNotNull(requestDto.getFirstName());
        assertEquals("Adrian", requestDto.getFirstName());
    }
}
