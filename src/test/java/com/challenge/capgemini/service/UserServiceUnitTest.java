package com.challenge.capgemini.service;

import com.challenge.capgemini.dto.response.UserDto;
import com.challenge.capgemini.entity.User;
import com.challenge.capgemini.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserServiceUnitTest {

    @Mock
    UserRepository userRepository;

    @InjectMocks
    UserServiceImpl userService;

    @Test
    void createUser_saveUser_returnNewUser() {

        //given
        UserDto userDto = new UserDto();
        userDto.setFirstName("Mostafa");
        userDto.setLastName("Golmohammadi");
        userDto.setBirthDate(LocalDate.of(1990, 8, 19));

        User user = User.builder().firstName(userDto.getFirstName()).lastName(userDto.getLastName()).birthDate(userDto.getBirthDate()).build();

        //when
        when(userRepository.save(user)).thenReturn(user);

        // assert
        user = userService.create(userDto);
        assertEquals(userDto.getFirstName(), user.getFirstName());
    }

}
