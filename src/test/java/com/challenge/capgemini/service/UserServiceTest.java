package com.challenge.capgemini.service;

import com.challenge.capgemini.Application;
import com.challenge.capgemini.dto.response.UserDto;
import com.challenge.capgemini.entity.User;
import com.challenge.capgemini.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ActiveProfiles("test")
@Sql(scripts = {"/db/changelog/sql/01-script-init.sql"})
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class UserServiceTest {
    @Autowired
    UserRepository userRepository;

    @Autowired
    UserService userService;

    @BeforeEach
    void deleteRecords() {
        userRepository.deleteAll();
    }

    @Test
    void createUser_saveUser_returnNewUser() {

        //given
        UserDto userDto = new UserDto();
        userDto.setFirstName("Mostafa");
        userDto.setLastName("Golmohammadi");
        userDto.setBirthDate(LocalDate.of(1990, 8, 19));

        //when
        User user = userService.create(userDto);

        //assert
        assertEquals(user.getFirstName(), userDto.getFirstName());
    }
}
