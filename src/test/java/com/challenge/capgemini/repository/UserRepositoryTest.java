package com.challenge.capgemini.repository;

import com.challenge.capgemini.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;

import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

@Sql(scripts = {"/db/changelog/sql/01-script-init.sql"})
@SpringBootTest
class UserRepositoryTest {
    @Autowired
    private UserRepository customerRepository;

    @Test
    void shouldCreateCustomer() {
        LocalDate birthDate = LocalDate.of(1990, 8, 19);
        //given
        User user = User.builder()
                .firstName("Alex")
                .lastName("Michel")
                .birthDate(birthDate)
                .build();
        customerRepository.save(user);

        //when
        List<User> users = customerRepository.findAll();


        //assert
        assertFalse(users.isEmpty());
        assertEquals(users.get(0).getFirstName(), user.getFirstName());
    }

}
