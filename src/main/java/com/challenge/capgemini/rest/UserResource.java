package com.challenge.capgemini.rest;

import com.challenge.capgemini.dto.mapper.UserMapper;
import com.challenge.capgemini.dto.request.UserRequestDto;
import com.challenge.capgemini.dto.response.UserDto;
import com.challenge.capgemini.service.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class UserResource {
    private final UserService userService;
    private final UserMapper mapper;

    public UserResource(UserService userService, UserMapper mapper) {
        this.userService = userService;
        this.mapper = mapper;
    }


    @GetMapping("/users")
    public ResponseEntity<List<UserDto>> getAll() {
        return ResponseEntity.ok(mapper.toDto(userService.findAll()));
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<UserDto> getById(@PathVariable Long id) {
        return ResponseEntity.ok(mapper.toDto(userService.getById(id)));
    }

    @PostMapping("/users")
    public ResponseEntity<UserDto> create(@RequestBody @Valid UserDto userDto) {
        return ResponseEntity.ok(mapper.toDto(userService.create(userDto)));
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<UserDto> update(@RequestBody @Valid UserDto userDto, @PathVariable Long id) {
        return ResponseEntity.ok(mapper.toDto(userService.update(userDto, id)));
    }

    @PatchMapping("/users/{id}")
    public ResponseEntity<UserDto> PartialUpdate(@RequestBody @Valid UserRequestDto requestDto, @PathVariable Long id) {
        return ResponseEntity.ok(mapper.toDto(userService.PartialUpdate(requestDto, id)));
    }
}
