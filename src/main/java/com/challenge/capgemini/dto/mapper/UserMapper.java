package com.challenge.capgemini.dto.mapper;

import com.challenge.capgemini.dto.response.UserDto;
import com.challenge.capgemini.entity.User;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserMapper extends EntityMapper<UserDto, User> {
}
