package com.challenge.capgemini.service;

import com.challenge.capgemini.dto.request.UserRequestDto;
import com.challenge.capgemini.dto.response.UserDto;
import com.challenge.capgemini.entity.User;

import java.util.List;

public interface UserService {

    List<User> findAll();

    User getById(Long id);

    User create(UserDto userDto);

    User update(UserDto userDto, Long id);

    User PartialUpdate(UserRequestDto requestDto, Long id);
}
