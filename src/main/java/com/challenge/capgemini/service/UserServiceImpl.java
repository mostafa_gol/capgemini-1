package com.challenge.capgemini.service;

import com.challenge.capgemini.dto.request.UserRequestDto;
import com.challenge.capgemini.dto.response.UserDto;
import com.challenge.capgemini.entity.User;
import com.challenge.capgemini.exception.CustomException;
import com.challenge.capgemini.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

@Slf4j
@Service
@Transactional
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User getById(Long id) {
        return userRepository.findById(id).orElseThrow(
                () -> new CustomException("User with id: " + id + " not found", HttpStatus.NOT_FOUND));
    }

    @Override
    public User create(UserDto userDto) {
        User user = User.builder().
                firstName(userDto.getFirstName()).
                lastName(userDto.getLastName()).
                birthDate(userDto.getBirthDate()).
                build();
        return userRepository.save(user);
    }

    @Override
    public User update(UserDto userDto, Long id) {
        User user = getById(id);
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setBirthDate(userDto.getBirthDate());
        return userRepository.save(user);
    }

    @Override
    public User PartialUpdate(UserRequestDto requestDto, Long id) {
        User user = getById(id);
        if (StringUtils.isNotEmpty(requestDto.getFirstName())) {
            user.setFirstName(requestDto.getFirstName());
        }
        if (StringUtils.isNotEmpty(requestDto.getLastName())) {
            user.setLastName(requestDto.getLastName());
        }
        if (Objects.nonNull(requestDto.getBirthDate())) {
            user.setBirthDate(requestDto.getBirthDate());
        }
        return user;
    }

}
