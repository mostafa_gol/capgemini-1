for running the application use these command: </br>

1- `mvn clean package`

2- `docker-compose -f docker-compose.yaml up --build`

The application use: <br>

spring boot 2.5.6 <br>
maven <br>
postgresql database for dev profile and h2 for testing <br>

Swagger address: http://localhost:8080/swagger-ui.html# <br>

You can see all APIs : `http://localhost:8080/swagger-ui.html#/` <br>
